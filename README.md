# Linux Syslog Daemon

Linux Syslog daemon based on https://github.com/troglobit/sysklogd which is porting the NetBSD and FreeBSD syslogd implementation to Linux.

## References
* Porting work from: https://github.com/troglobit/sysklogd
* This is the continuation of the original sysklogd by Dr. G.W. Wettstein and Martin Schulze. Currently maintained, and almost completely rewritten with the latest DNA strands from NetBSD and FreeBSD, by Joachim Wiberg. Please file bug reports, or send pull requests for bug fixes and proposed extensions at GitHub.
