install(FILES ${CMAKE_BINARY_DIR}/config/syslog.conf DESTINATION /etc)
if(WITH_SYSTEMD)
    install(FILES ${CMAKE_BINARY_DIR}/config/syslogd.service DESTINATION /etc/systemd/system)
endif()